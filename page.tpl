<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$language}" xml:lang="{$language}">
<head profile="http://gmpg.org/xfn/11">
<title>{$head_title}</title>
{$head}{$styles}
<script type="text/javascript" src="{$directory}/js/prototype.js"></script>
<script type="text/javascript" src="{$directory}/js/effects.js"></script>
<script type="text/javascript">{* Needed to avoid Flash of Unstyle Content in IE *} </script>
</head>
<body{$onload_attributes}>
<div id="page">
  <div id="header">
    {if $site_name}<h1><a href="./" title="Home">{$site_name}</a></h1>{/if} 
    {if $site_slogan}<p class="description">{$site_slogan}</p>{/if}
    {$header}
  {if $primary_links}{theme function='primary' links=$primary_links}{/if}
  </div>
  <hr />
  <div class="primary">
    {if $mission}<div id="mission">{$mission}</div>{/if}
    {$breadcrumb}
    <div class="tabs">{$tabs}</div>
    <div class="pagetitle">
      <h2>{$title}</h2>
    </div>
    {$help} {$messages} {$content} </div>
  <hr />
  <div class="secondary">
    <div class="sb-search">
      <h2>Search</h2>
      {$search_box}
    </div>
    {if $sidebar_right}
    {$sidebar_right}
    {/if}
  </div>
  <br class="clear" />
</div>
<!-- Close Page -->
<hr />
<p id="footer"><small>
  {if $footer}{$footer_message}<br />{/if}
  {$site_name} is powered by <a href="http://drupal.org" title="Community Plumbing">Drupal </a> and <a href="http://binarybonsai.com/k2/" title="Is to WordPress what math is to reality; hard to understand.">K2</a> by <a href="http://binarybonsai.com" title="Michael Heilemann">Michael</a> and <a href="http://chrisjdavis.org" title="Chris J Davis">Chris</a> </small></p>
{$closure}
</body>
</html>