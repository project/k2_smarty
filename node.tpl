<div class="item entry{if $sticky}  sticky{/if}">
  {if $picture}{$picture}{/if}
  <div class="itemhead">
    {if $page == 0}
    <h3><a href="{$node_url}" rel="bookmark" title="Permanent Link to '{$title}'">{$title}</a></h3>
    {/if}
    <small class="metadata">
      <span class="chronodata">{$submitted}<span class="links"> {$links}</span></span>{if $terms} <span class="tagdata">Tags: {$terms}.</span>{/if} </small>
  </div>
  <div class="itemtext"> {$content} </div>
</div>
