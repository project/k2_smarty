<?php
function k2_smarty_primary($items = array()) {
  $output = '<ul class="menu">';
    if (!empty($items)) {
    foreach ($items as $item) {
      $output .= '<li class="page_item">'. $item .'</li>';
    }
  }
  $output .= '</ul>';
  return $output;
}

function k2_smarty_regions() {
  return array(
    'right' => t('right sidebar'),
    'content' => t('content'),
    'header' => t('header'),
    'footer' => t('footer')
  );
}
?>
