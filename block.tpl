  <div class="block block-{$block->module}" id="block-{$block->module}-{$block->delta}">
    {if $block->subject}<h2>{$block->subject}</h2>{/if}
    <div class="content">{$block->content}</div>
  </div>
